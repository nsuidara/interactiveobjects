﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionUI : MonoBehaviour
{
    public void OnClick()
    {
        IAction[] actions = GetComponents<IAction>();
        foreach(IAction action in actions)
        {
            action.Run();
        }
    }
}
