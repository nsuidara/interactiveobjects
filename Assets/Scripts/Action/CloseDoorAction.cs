﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseDoorAction : MonoBehaviour, IAction
{
    public GameObject door;
    public Vector3 move = new Vector3(0, 1, 0);

    private Vector3 startPosition;

    private void Start()
    {
        startPosition = door.transform.position;
    }

    public void Run()
    {
        if (startPosition != door.transform.position)
        {
            door.transform.position -= move;
        }
    }
}

